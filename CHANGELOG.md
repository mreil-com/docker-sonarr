# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2019-02-20
### Added
- First release, contains Sonarr 2.0.0

[Unreleased]: ../../src
[2.0.2]: ../../branches/compare/rel-2.0.1..rel-2.0.2#diff
[2.0.1]: ../../src/v2.0.1
