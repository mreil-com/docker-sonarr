# docker-sonarr

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-sonarr.svg)](https://bitbucket.org/mreil-com/docker-sonarr/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Sonarr](https://github.com/Sonarr/Sonarr) image

Based on [mreil/ubuntu-base][ubuntu-base].

## Usage

    docker run -i -t \
    -p 8989:8989 \
    -v ${DATA_DIR}/sonarr/NzbDrone:/root/.config/NzbDrone \
    mreil/sonarr

### Environment variables

| Variable | Description                                  | Default          |
|----------|----------------------------------------------|------------------|
| DATA_DIR | Where Sonarr keeps its data and config file. | /mnt/sonarr-data |

## Build

    ./gradlew build

### Update Gradle version

    ./gradlew wrapper --gradle-version [VERSION]

## Releases

See [hub.docker.com](https://hub.docker.com/r/mreil/sonarr/tags/).

## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base
