#!/usr/bin/env bash

set -e

# Install dependencies
apt-get update -y
apt-get install -y wget gnupg ca-certificates mediainfo libicu74

cd /root
wget -O sonarr.tgz "https://services.sonarr.tv/v1/download/main/latest?version=4&os=linux&arch=x64"
tar xf sonarr.tgz
rm sonarr.tgz

./Sonarr/Sonarr /?
